package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
	"net/http"
	"os"
	"strings"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

type command struct {
	Text      string `json:"text"`
	NameSpace string `json:"namespace"`
	PodName   string `json:"podname"`
}
type LogStreamer struct {
	buffer bytes.Buffer
}

func (log *LogStreamer) String() string {
	return log.buffer.String()
}
func (log *LogStreamer) Write(p []byte) (n int, err error) {
	a := strings.TrimSpace(string(p))
	log.buffer.WriteString(a)
	return len(p), nil
}
func connectToK8SClusterWithConfigFile() (*kubernetes.Clientset, *rest.Config, error) {
	//this file configuration is static !! it will be dynamically created when integrating ms.
	const configFilePath = "/.kube/config.yaml"
	userHomeDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Printf("error getting user home dir: %v\n", err)
		os.Exit(1)
	}
	kubeConfigPath := userHomeDir + configFilePath
	kubeConfig, err := clientcmd.BuildConfigFromFlags("", kubeConfigPath)
	if err != nil {
		return nil, nil, err
		os.Exit(1)
	}
	k8sClient, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		return nil, kubeConfig, err
		os.Exit(1)
	}
	return k8sClient, kubeConfig, nil
}
func executeRemoteCommandOnResource(command string, kubeConfig *rest.Config, k8sClient *kubernetes.Clientset, namespace string, podName string) (remotecommand.Executor, int, error) {
	const subResource = "exec"
	const resource = "pods"
	request := k8sClient.CoreV1().RESTClient().Post().Resource(resource).
		Name(podName).
		Namespace(namespace).
		SubResource(subResource)
	requestOption := &v1.PodExecOptions{
		Command: []string{"sh", "-c", command},
		Stdin:   true,
		Stdout:  true,
		Stderr:  true,
		TTY:     true,
	}
	request.VersionedParams(
		requestOption,
		scheme.ParameterCodec,
	)
	exec, err := remotecommand.NewSPDYExecutor(kubeConfig, "POST", request.URL())
	if err != nil {

		return nil, 0, fmt.Errorf("could not exec Text: %w", err)
	}
	return exec, 1, nil
}
func streamExecutionResult(exec remotecommand.Executor) (string, int, error) {
	var streamErr error
	log := &LogStreamer{}
	streamErr = exec.Stream(remotecommand.StreamOptions{
		Stdin:  os.Stdin,
		Stdout: log,
		Stderr: nil,
		Tty:    true,
	})
	if streamErr != nil {
		return log.String(), 0, streamErr
	}
	return log.String(), 1, nil
}
func executeCommand(commandToExecute command) (string, int, error) {
	clientSet, kubeConfig, err := connectToK8SClusterWithConfigFile()
	if err != nil {
		return "", 0, err

	}
	remoteCommandExecution, status, err := executeRemoteCommandOnResource(commandToExecute.Text, kubeConfig, clientSet, commandToExecute.NameSpace, commandToExecute.PodName)
	if err != nil {
		return "", status, fmt.Errorf("could not exec Text: %w", err)
	}

	result, status, err := streamExecutionResult(remoteCommandExecution)
	if err != nil {
		return result, status, err

	} else {
		return result, status, err
	}
}

// Rest
func executeCommandRequest(context *gin.Context) {
	var newCommand command
	if err := context.BindJSON(&newCommand); err != nil {
		return
	}
	result, _, _ := executeCommand(newCommand)
	context.IndentedJSON(200, result)

}

// WebSocket
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func executeCommandWS(w http.ResponseWriter, r *http.Request) {

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		var newCommand command
		json.Unmarshal(msg, &newCommand)
		result, _, _ := executeCommand(newCommand)
		data := []byte(result)
		conn.WriteMessage(t, data)

	}
}

func main() {
	router := gin.Default()
	router.POST("/api/v1/execute", executeCommandRequest)
	router.GET("/api/v1/executeWS", func(c *gin.Context) {
		executeCommandWS(c.Writer, c.Request)
	})

	router.Run("localhost:8080")
}
